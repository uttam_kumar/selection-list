package com.example.uttam.demoselectionlist;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class TeamDetails extends AppCompatActivity {

    ImageView backArrow;
    TextView heading;
    TextView nametxt;
    TextView placetxt;

    String tName;
    String tPlace;
    String tCheck,tVol,tMicro;
    int itemPosition=-1;

    ImageView volumeImg, microphoneImg, checkImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_details);

        Intent intent=getIntent();
        setResult(1,intent);
        tName=intent.getStringExtra("teamName");
        tPlace=intent.getStringExtra("teamPlace");
        tCheck=intent.getStringExtra("checked");
        tVol=intent.getStringExtra("volume");
        Log.d("checked", "checked: "+tCheck);
        tMicro=intent.getStringExtra("microphone");
        itemPosition=intent.getIntExtra("listItemPosition",-1);


        backArrow=findViewById(R.id.backArrowId);
        heading=findViewById(R.id.headingId);

        nametxt=findViewById(R.id.nameId);
        placetxt=findViewById(R.id.placeId);

        volumeImg=findViewById(R.id.secondActivityVolume);
        microphoneImg=findViewById(R.id.secondActivityMicrophone);
        checkImg=findViewById(R.id.secondActivityCheckbox);


        heading.setText(tName);
        heading.setSingleLine(true);
        heading.setEllipsize(TextUtils.TruncateAt.END);

        nametxt.setText("Team Name: "+tName);
        nametxt.setSingleLine(true);
        nametxt.setEllipsize(TextUtils.TruncateAt.END);


        placetxt.setText("Place: "+tPlace);
        placetxt.setSingleLine(true);
        placetxt.setEllipsize(TextUtils.TruncateAt.END);

        if(tCheck.equals("1")){
            checkImg.setImageResource(R.drawable.icon_gps_on);
        }else{
            checkImg.setImageResource(R.drawable.icon_gps_off);
        }

        if(tVol.equals("1")){
            volumeImg.setImageResource(R.drawable.icon_home_volume_on);
        }else{
            volumeImg.setImageResource(R.drawable.icon_home_volume_off);
        }

        if(tMicro.equals("1")){
            microphoneImg.setImageResource(R.drawable.icon_home_microphone_on);
        }else{
            microphoneImg.setImageResource(R.drawable.icon_home_microphone_off);
        }

        checkImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkImg.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.icon_gps_on).getConstantState()){
                    checkImg.setImageResource(R.drawable.icon_gps_off);
                    update_check(String.valueOf(itemPosition),"0");
                    tCheck="0";
                }else if(checkImg.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.icon_gps_off).getConstantState()){
                    checkImg.setImageResource(R.drawable.icon_gps_on);
                    update_check(String.valueOf(itemPosition),"1");
                    tCheck="1";
                }
            }
        });

        volumeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(volumeImg.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.icon_home_volume_on).getConstantState()){
                    volumeImg.setImageResource(R.drawable.icon_home_volume_off);
                    update_volume(String.valueOf(itemPosition),"0");
                    tVol="0";
                }else if(volumeImg.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.icon_home_volume_off).getConstantState()){
                    volumeImg.setImageResource(R.drawable.icon_home_volume_on);
                    update_volume(String.valueOf(itemPosition),"1");
                    tVol="1";
                }
            }
        });

        microphoneImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(microphoneImg.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.icon_home_microphone_on).getConstantState()){
                    microphoneImg.setImageResource(R.drawable.icon_home_microphone_off);
                    update_micro(String.valueOf(itemPosition),"0");
                    tMicro="0";
                }else if(microphoneImg.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.icon_home_microphone_off).getConstantState()){
                    microphoneImg.setImageResource(R.drawable.icon_home_microphone_on);
                    update_micro(String.valueOf(itemPosition),"1");
                    tMicro="1";
                }
            }
        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void update_micro(String pId, String vmcId){

        String methodName="update_micro";
        //progressDialog.show()
        ApiHelper apiHelper= new ApiHelper(this, pId,vmcId,methodName);
        apiHelper.execute();
        //progressDialog.dismiss()
    }

    public void update_volume(String pId,String vmcId){

        String methodName="update_volume";
        //progressDialog.show()
        ApiHelper apiHelper= new ApiHelper(this, pId,vmcId,methodName);
        apiHelper.execute();
        //progressDialog.dismiss()
    }

    public void update_check(String pId,String vmcId){

        String methodName="update_check";
        //progressDialog.show()
        ApiHelper apiHelper= new ApiHelper(this, pId,vmcId,methodName);
        apiHelper.execute();
        //progressDialog.dismiss()
    }
}
