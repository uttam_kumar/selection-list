//package com.example.uttam.demoselectionlist;
//
//import android.annotation.TargetApi;
//import android.app.Fragment;
//import android.content.Intent;
//import android.graphics.Color;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.annotation.RequiresApi;
//import android.text.Editable;
//import android.text.TextUtils;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.CheckBox;
//import android.widget.CompoundButton;
//import android.widget.EditText;
//import android.widget.ListView;
//import android.widget.SearchView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Locale;
//import java.util.Random;
//
//
//public class SecondFragment extends Fragment {
//
//    private View rootView;
//    private ArrayAdapter<ItemList> adapter;
//    private List<ItemList> TeamItem;
//    private ListView lv;
//    ArrayList<ItemList> mAllData=new ArrayList<ItemList>();
//    int m=30;
//    String[] teamList;
//    String[] teamPlace;
//    CheckBox selectAllCheck;
//    SearchView searchView;
//    boolean[] checkedIcon;
//    boolean[] volumeIcon;
//    boolean[] microphoneIcon;
//    EditText et;
//    int[] itemPosition;
//    ItemList currentPosition;
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//
//        //Random random = new Random();
//        //int m = random.nextInt(50 - 5) + 5;
//        teamList= new String[m];
//        teamPlace=new String[m];
//        checkedIcon=new boolean[m];
//        volumeIcon=new boolean[m];
//        microphoneIcon=new boolean[m];
//        itemPosition=new int[m];
//
//
//        for(int i=0;i<m;i++){
//            teamList[i]="Worker's Team "+(i+1);
//            teamPlace[i]="Team Place "+(i+1);
//
//            itemPosition[i]=i;
//
//            checkedIcon[i]=true;
//            volumeIcon[i]=true;
//            microphoneIcon[i]=true;
//        }
//
//        rootView = inflater.inflate(R.layout.fragment_second, container, false);
//        lv = (ListView)rootView.findViewById(R.id.teamsListViewId);
//        selectAllCheck=rootView.findViewById(R.id.selectAllCheckId);
//        et = (EditText)rootView.findViewById(R.id.searchViewId);
//
//
//        selectAllCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                et.setText("");
//                if(selectAllCheck.isChecked()){
//                    for(int i=0;i<m;i++){
//                        if(!checkedIcon[i]){
//                            checkedIcon[i]=true;
//                        }
//                    }
////                    adapter = new MyCustomListAdapter(getActivity().getApplicationContext(),  R.layout.sample_view, TeamItem);
////                    lv.setAdapter(adapter);
//                }else{
//                    int t=0;
//                    for(int i=0;i<m;i++){
//                        if(checkedIcon[i]){
//                            if(i==m-1){
//                                t=1;
//                            }
//                            //checkedIcon[i]=false;
//                        }else{
//                            t=2;
//                            break;
//                        }
//                    }
//
//                    if(t==1){
//                        for(int i=0;i<m;i++){
//                            checkedIcon[i]=false;
//                        }
//                    }
//
////                    adapter = new MyCustomListAdapter(getActivity().getApplicationContext(),  R.layout.sample_view, TeamItem);
////                    lv.setAdapter(adapter);
//                }
//
//                TeamItem = new ArrayList<ItemList>();
//
//                for(int i=0;i<m;i++){
//                    TeamItem.add(new ItemList(teamList[i],teamPlace[i],microphoneIcon[i],volumeIcon[i],checkedIcon[i],itemPosition[i]));
//                }
//
//                mAllData.addAll(TeamItem);
//                adapter = new MyCustomListAdapter(getActivity().getApplicationContext(),  R.layout.sample_view, TeamItem,microphoneIcon,volumeIcon,checkedIcon,itemPosition);
//                lv.setAdapter(adapter);
//            }
//        });
//
//
//        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @RequiresApi(api = Build.VERSION_CODES.M)
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                String name=teamList[i];
//                String place=teamPlace[i];
//                boolean checked=checkedIcon[i];
//                boolean vol=volumeIcon[i];
//                boolean micro=microphoneIcon[i];
//
//                Intent intent=new Intent(getContext(),TeamDetails.class);
//                intent.putExtra("teamName",name);
//                intent.putExtra("teamPlace",place);
//                intent.putExtra("checked",checked);
//                intent.putExtra("volume",vol);
//                intent.putExtra("microphone", micro);
//                intent.putExtra("listItemPosition",i);
//
//                startActivity(intent);
//                //Toast.makeText(getContext(),"clicked: "+i,Toast.LENGTH_LONG).show();
//            }
//        });
//        populateDrinksList();
//        doSearch();
//        return rootView;
//    }
//
//
//    private void doSearch() {
//        et.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                String text = et.getText().toString().toLowerCase(Locale.getDefault());
//                filter(text);
//            }
//        });
//    }
//
//
//    private void populateDrinksList() {
//        TeamItem = new ArrayList<ItemList>();
//
//        for(int i=0;i<m;i++){
//            TeamItem.add(new ItemList(teamList[i],teamPlace[i],microphoneIcon[i],volumeIcon[i],checkedIcon[i],itemPosition[i]));
//        }
//
//        mAllData.addAll(TeamItem);
//        adapter = new MyCustomListAdapter(getActivity().getApplicationContext(),  R.layout.sample_view, TeamItem,microphoneIcon,volumeIcon,checkedIcon,itemPosition);
//        lv.setAdapter(adapter);
//    }
//
//
//    public void filter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        TeamItem .clear();
//        if (charText.length() == 0) {
//            TeamItem.addAll(mAllData);
//        } else {
//            for (ItemList wp : mAllData) {
//                if (wp.gettName().toLowerCase(Locale.getDefault())
//                        .contains(charText)) {
//                    TeamItem .add(wp);
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }
//
//    private void notifyDataSetChanged() {
//        adapter.notifyDataSetChanged();
//    }
//
//}




//        for (i in 1 until m) {
//
//            itemPosition[i]=i-0
//
//            val baseUrlLogIn = "http://192.168.207.24/retrieve.php?Id=1";
//            Log.d("URL", "callLoginApi: baseUrlLogIn: "+baseUrlLogIn)
//
//            val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Response.Listener { s ->
//                Log.d("SUCCESS", "RequestSuccessVal: " + s)
//                var jsonObject = JSONObject(s)
//                Log.d("Status", "StatVal: " + jsonObject)
//                var status=jsonObject.get("Status")
//                Log.d("st", "st: " + status)
//                if(status.equals("ok")){
//                    var responseValue=jsonObject.get("Responsvalue")
//                    var arr= JSONArray("["+responseValue.toString()+"]")
//
//                    teamList[i] =arr.getJSONObject(0).getString("team_name")
//                    Log.d("team_name", "team_name: " + teamList[i])
//                    teamPlace[i]=arr.getJSONObject(0).getString("team_place")
//                    Log.d("teamP", "teamP: " + teamPlace[i])
//                    volumeIcon[i]=arr.getJSONObject(0).getString("volume")
//                    Log.d("teamV", "teamV: " + volumeIcon[i])
//                    microphoneIcon[i]=arr.getJSONObject(0).getString("microphone")
//                    Log.d("teamM", "teamM: " + microphoneIcon[i])
//                    checkedIcon[i]=arr.getJSONObject(0).getString("checkbox")
//                    Log.d("teamc", "teamc: " + checkedIcon[i])
//
//
//                }else{
//                    //progressDialog.dismiss()
//                    Toast.makeText(context, "Error else", Toast.LENGTH_SHORT).show();
//                }
//
//            }, Response.ErrorListener { e ->
//                //progressDialog.dismiss()
//                Toast.makeText(context, "Error response", Toast.LENGTH_SHORT).show();
//                Log.d("ErrorListener","ErrorListener"+e.message)
//            }) {
//                override fun getParams(): Map<String, String> {
//                    var params= HashMap<String,String>()
//                    params.put("Id","1")
//                    return  params
//                }
//            }
//            val  requestQueue = Volley.newRequestQueue(context)
//            requestQueue.add<String>(stringRequest)
//
//        }
