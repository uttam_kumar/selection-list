package com.example.uttam.demoselectionlist;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MyCustomListAdapter extends ArrayAdapter<ItemList> {

    ImageView microImage;
    ImageView volumeImage;
    ImageView checkImage;
    TextView name;
    TextView place;
    ItemList currentPosition;
    String[] checkedAllItem;
    String[] volumeAllItem;
    String[] microphoneAllItem;
    int[] itemPosAllItem;
    Context context;
    List<ItemList> data;
    int layout;
    View arrayView;

    private ArrayList<ItemList> arraylist;

//    public MyCustomListAdapter(Context context, int q){
////        super(context,q);
////        this.context=context;
////        //Default constructor
////    }

    public MyCustomListAdapter(Context context, int layoutId, List<ItemList> items,String[] mIcon,String[] vIcon,String[] cIcon,int[] itmPosition) {
        super(context, layoutId, items);
        this.context=context;
        this.checkedAllItem=cIcon;
        this.microphoneAllItem=mIcon;
        this.volumeAllItem=vIcon;
        this.itemPosAllItem=itmPosition;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        arrayView = convertView;
        if(arrayView == null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            arrayView = vi.inflate(R.layout.sample_view, parent, false);
        }

        currentPosition = getItem(position);
        //seconfFrag=new SecondFragment2();

        if(currentPosition != null){
            microImage = (ImageView)arrayView.findViewById(R.id.microPhoneIconId);
            volumeImage = (ImageView)arrayView.findViewById(R.id.volumeIconId);
            checkImage = (ImageView)arrayView.findViewById(R.id.checkboxIconId);

            name = (TextView)arrayView.findViewById(R.id.nameTextId);
            name.setText(currentPosition.gettName());
            name.setSingleLine(true);
            name.setEllipsize(TextUtils.TruncateAt.END);

            place = (TextView)arrayView.findViewById(R.id.placeTextId);
            place.setText(currentPosition.gettPlace());
            place.setSingleLine(true);
            place.setEllipsize(TextUtils.TruncateAt.END);

            setItem(position);
        }

        checkImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (Integer) view.getTag();
                //Toast.makeText(getContext(),"clicked checkImage id: "+pos,Toast.LENGTH_SHORT).show();
                if(checkedAllItem[pos]=="1"){
                    ((ImageView) view).setImageResource(R.drawable.icon_gps_off);
                    update_check(String.valueOf(pos+1),"0");
                    checkedAllItem[pos]="0";
                }else{
                    ((ImageView) view).setImageResource(R.drawable.icon_gps_on);
                    update_check(String.valueOf(pos+1),"1");
                    checkedAllItem[pos]="1";
                }

                new SecondFragment2().toast();
                //calling enable method from secondfragment2 class
            }
        });
        volumeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (Integer) view.getTag();
                if(volumeAllItem[pos]=="1"){
                    ((ImageView) view).setImageResource(R.drawable.icon_home_volume_off);
                    update_volume(String.valueOf(pos+1),"0");
                    volumeAllItem[pos]="0";
                }else{
                    ((ImageView) view).setImageResource(R.drawable.icon_home_volume_on);
                    update_volume(String.valueOf(pos+1),"1");
                    volumeAllItem[pos]="1";
                }
            }
        });

        microImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (Integer) view.getTag();
                //Toast.makeText(getContext(),"clicked microImage id: ",Toast.LENGTH_SHORT).show();
                if(microphoneAllItem[pos]=="1"){
                    ((ImageView) view).setImageResource(R.drawable.icon_home_microphone_off);
                    update_micro(String.valueOf(pos+1),"0");
                    microphoneAllItem[pos]="0";
                }else{
                    ((ImageView) view).setImageResource(R.drawable.icon_home_microphone_on);
                    update_micro(String.valueOf(pos+1),"1");
                    microphoneAllItem[pos]="1";
                }
            }
        });
        return arrayView;
    }

    public void setItem(int t){
        if(volumeAllItem[t].equals("1")){
            Log.d("TAG1", "volumeAllItem: true");
            volumeImage.setImageResource(R.drawable.icon_home_volume_on);
            volumeImage.setTag(t);
        }else{
            Log.d("TAG1", "volumeAllItem: false");;
            volumeImage.setImageResource(R.drawable.icon_home_volume_off);
            volumeImage.setTag(t);
        }

        if(microphoneAllItem[t].equals("1")){
            Log.d("TAG1", "microphoneAllItem: true");
            microImage.setImageResource(R.drawable.icon_home_microphone_on);
            microImage.setTag(t);
        }else{
            Log.d("TAG1", "microphoneAllItem: false");
            microImage.setImageResource(R.drawable.icon_home_microphone_off);
            microImage.setTag(t);
        }

        if(checkedAllItem[t].equals("1")){
            Log.d("TAG1", "checkedAllItem: true");
            checkImage.setImageResource(R.drawable.icon_gps_on);
            checkImage.setTag(t);
        }else{
            Log.d("TAG1", "checkedAllItem: false");
            checkImage.setImageResource(R.drawable.icon_gps_off);
            checkImage.setTag(t);
        }
    }

    public void update_micro(String pId,String vmcId){

        String methodName="update_micro";
        //progressDialog.show()
        ApiHelper apiHelper= new ApiHelper(getContext(), pId,vmcId,methodName);
        apiHelper.execute();
        //progressDialog.dismiss()
    }

    public void update_volume(String pId,String vmcId){

        String methodName="update_volume";
        //progressDialog.show()
        ApiHelper apiHelper= new ApiHelper(getContext(), pId,vmcId,methodName);
        apiHelper.execute();
        //progressDialog.dismiss()
    }

    public void update_check(String pId,String vmcId){

        String methodName="update_check";
        //progressDialog.show()
        ApiHelper apiHelper= new ApiHelper(getContext(), pId,vmcId,methodName);
        apiHelper.execute();
        //progressDialog.dismiss()
    }

}