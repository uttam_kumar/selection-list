package com.example.uttam.demoselectionlist

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat.startActivity
import android.util.Log
import android.widget.*
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.Response.Listener
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap

class ApiHelper() : AsyncTask<Void, Void, String>() {

    lateinit var context:Context
    lateinit var Id:String
    lateinit var volMicCheckId:String
    lateinit var method:String
    var listView: ListView?=null

    lateinit var TeamItem: List<ItemList>

    lateinit var teamNN: Array<String?>
    lateinit var teamPP: Array<String?>
    lateinit var checkII: Array<String?>
    lateinit var volII: Array<String?>
    lateinit var microII: Array<String?>
    lateinit var positionII: IntArray

    lateinit var data:String
    lateinit var searchText:String

    var arrayListviewAdapter= arrayListOf<ItemList>()
    lateinit var listDataAdapter: ListAdapter

    //constructor for updating each of each list item button
    constructor(myContext: Context, id:String, vol:String, methodName:String):this(){
        context=myContext
        Id=id
        volMicCheckId=vol
        method=methodName
        //progressDialog=ProgressDialog(context)
    }

    //getting all data from database and set it to list
    constructor(myContext: Context, listV: ListView,methodName:String):this(){
        context=myContext
        listView=listV
        method=methodName
        //progressDialog=ProgressDialog(context)
    }

    //getting all search data from database and set it to list
    constructor(myContext: Context, listV: ListView,methodName:String,search:String):this(){
        context=myContext
        listView=listV
        searchText=search
        method=methodName
        //progressDialog=ProgressDialog(context)
    }


    //default value for location, volume & microphone
    constructor(myContext: Context,methodName:String):this(){
        context=myContext
        method=methodName
    }

    constructor(myContext: Context, id: Int,methodName:String):this(){
        context=myContext
        Id=(id+1).toString()
        method=methodName
        //progressDialog=ProgressDialog(context)
    }


    //updating all checkbox
    constructor(myContext: Context, data: String,methodName:String):this(){
        context=myContext
        this.data=data
        method=methodName
        //progressDialog=ProgressDialog(context)
    }

    override fun onPreExecute() {
        super.onPreExecute()
       // progressDialog.setMessage("Loading...")
        //progressDialog.show()
    }

    override fun doInBackground(vararg p0: Void?): String {
        if (method.equals("update_check")){
            callUpdateCheckApi()
        }else if (method.equals("update_volume")){
            callUpdateVolumeApi()
        }else if (method.equals("update_micro")){
            callUpdateMicroApi()
        }else if (method.equals("second_activity")){
            //callSecondActivityApi()
        }else if (method.equals("retrieveAllData")){
            callRetrieveDataApi()
        }else if (method.equals("select_all_gps")){
            callSelectAllGpsCheckBoxApi()
        }else if (method.equals("select_all_microphone")){
            callSelectAllMicrophoneCheckBoxDataApi()
        }else if (method.equals("select_all_volume")){
            callSelectAllVolumeCheckBoxDataApi()
        }else if (method.equals("defaultAllOff")){
            callDefaultAllOffApi()
        }else if (method.equals("search")){
            callSearchApi()
        }

        return ""
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        //progressDialog.dismiss()
    }

    //to make true or false all the list item volume
    private fun callSelectAllVolumeCheckBoxDataApi() {
        val baseUrlLogIn = "http://192.168.207.24/select_all_volume.php?data="+data
        Log.d("URL", "callSelectAllVolumeCheckBoxDataApi : "+baseUrlLogIn)

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Listener { s ->
            Log.d("SUCCESS", "callSelectAllVolumeCheckBoxDataApi: " + s)
            var jsonObject = JSONObject(s)
            //Log.d("Status", "StatVal: " + jsonObject)
            var status=jsonObject.get("Status")
            //Log.d("st", "st: " + status)
            if(status.equals("ok")){
                //Toast.makeText(context, "status ok", Toast.LENGTH_SHORT).show();

            }else{
                //progressDialog.dismiss()
                Toast.makeText(context, "Error else", Toast.LENGTH_SHORT).show();
            }

        }, Response.ErrorListener { e ->
            //progressDialog.dismiss()
            Toast.makeText(context, "Error response", Toast.LENGTH_SHORT).show();
            Log.d("ErrorListener","ErrorListener"+e.message)
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                params.put("data",data)
                return  params
            }
        }
        val  requestQueue = Volley.newRequestQueue(context)
        requestQueue.add<String>(stringRequest)
    }

    //to make true or false all the list item microphone
    private fun callSelectAllMicrophoneCheckBoxDataApi() {
        val baseUrlLogIn = "http://192.168.207.24/select_all_microphone.php?data="+data
        //Log.d("URL", "callSelectAllMicrophoneCheckBoxDataApi : "+baseUrlLogIn)

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Listener { s ->
            //Log.d("SUCCESS", "callSelectAllMicrophoneCheckBoxDataApi: " + s)
            var jsonObject = JSONObject(s)
            var status=jsonObject.get("Status")
            if(status.equals("ok")){

            }else{
                Toast.makeText(context, "Error else", Toast.LENGTH_SHORT).show();
            }

        }, Response.ErrorListener { e ->
            Toast.makeText(context, "Error response", Toast.LENGTH_SHORT).show();
            Log.d("ErrorListener","ErrorListener"+e.message)
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                params.put("data",data)
                return  params
            }
        }
        val  requestQueue = Volley.newRequestQueue(context)
        requestQueue.add<String>(stringRequest)
    }

    //to make true or false all the list item gps
    private fun callSelectAllGpsCheckBoxApi() {
        val baseUrlLogIn = "http://192.168.207.24/select_all_gps.php?data="+data
        Log.d("URL", "callSelectAllGpsCheckBoxApi : "+baseUrlLogIn)

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Listener { s ->
            Log.d("SUCCESS", "callSelectAllGpsCheckBoxApi: " + s)
            var jsonObject = JSONObject(s)
            //Log.d("Status", "StatVal: " + jsonObject)
            var status=jsonObject.get("Status")
            //Log.d("st", "st: " + status)
            if(status.equals("ok")){
                //Toast.makeText(context, "status ok", Toast.LENGTH_SHORT).show();

            }else{
                //progressDialog.dismiss()
                Toast.makeText(context, "Error else", Toast.LENGTH_SHORT).show();
            }

        }, Response.ErrorListener { e ->
            //progressDialog.dismiss()
            Toast.makeText(context, "Error response", Toast.LENGTH_SHORT).show();
            Log.d("ErrorListener","ErrorListener"+e.message)
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                params.put("data",data)
                return  params
            }
        }
        val  requestQueue = Volley.newRequestQueue(context)
        requestQueue.add<String>(stringRequest)
    }

    //to retrieve all data into list
    private fun callRetrieveDataApi() {
        val baseUrlLogIn = "http://192.168.207.24/retrieve.php?"
        Log.d("URL", "callRetrieveDataApi: "+baseUrlLogIn)

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Listener { s ->
            Log.d("SUCCESS", "callRetrieveDataApi: " + s)
            var jsonObject = JSONObject(s)
            //Log.d("Status", "StatVal: " + jsonObject)
            var status=jsonObject.get("Status")
            Log.d("st", "st: " + status)
            if(status.equals("ok")){
                var responseValue=jsonObject.get("Responsvalue")
                var arrayLength=jsonObject.get("RowNumber")
                var ar= JSONArray("["+responseValue.toString()+"]")

                var arr=ar.getJSONArray(0)
                Log.d("gg", "gg: " + arr)

                teamNN = arrayOfNulls<String>(arrayLength as Int)
                teamPP = arrayOfNulls<String>(arrayLength as Int)
                checkII = arrayOfNulls<String>(arrayLength as Int)
                volII = arrayOfNulls<String>(arrayLength as Int)
                microII = arrayOfNulls<String>(arrayLength as Int)
                positionII = IntArray(arrayLength as Int)

                for (i in 0 until arrayLength as Int) {
                    positionII[i]=arr.getJSONObject(i).getInt("id")
                    Log.d("positionII", "positionII: " + positionII[i])
                    teamNN[i]=arr.getJSONObject(i).getString("team_name")
                    Log.d("team_name", "team_name: " + teamNN[i])
                    teamPP[i]=arr.getJSONObject(i).getString("team_place")
                    Log.d("teamP", "teamP: " + teamPP[i])
                    volII[i]=arr.getJSONObject(i).getString("volume")
                    Log.d("teamV", "teamV: " + volII[i])
                    microII[i]=arr.getJSONObject(i).getString("microphone")
                    Log.d("teamM", "teamM: " + microII[i])
                    checkII[i]=arr.getJSONObject(i).getString("checkbox")
                    Log.d("teamc", "teamc: " + checkII[i])

                    arrayListviewAdapter.add(ItemList(teamNN[i],teamPP[i],microII[i],volII[i],checkII[i],i))
                }
                //progressDialog.dismiss()
                listDataAdapter=MyCustomListAdapter(context,  R.layout.sample_view, arrayListviewAdapter,microII,volII,checkII,positionII)
                listView!!.adapter=listDataAdapter
                //Log.d("ARRAYLIS","SIZE: "+arrayListviewAdapter.size)
            }else{
                //progressDialog.dismiss()
                Toast.makeText(context, "Error else", Toast.LENGTH_SHORT).show();
            }
        }, Response.ErrorListener { e ->
            // progressDialog.dismiss()
            Toast.makeText(context, "Error response", Toast.LENGTH_SHORT).show();
            Log.d("ErrorListener","ErrorListener"+e.message)
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                return  params
            }
        }
        var  requestQueue = Volley.newRequestQueue(context)
        requestQueue.add<String>(stringRequest)
    }

    //to search all data into list
    private fun callSearchApi() {
        if(searchText==""){
            callRetrieveDataApi()
        }else {
            val baseUrlLogIn = "http://192.168.207.24/search.php?content=" + searchText
            Log.d("URL", "callSearchApi: " + baseUrlLogIn)
            arrayListviewAdapter.clear()


            val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Listener { s ->
                Log.d("SUCCESS", "callSearchApi: " + s)
                var jsonObject = JSONObject(s)
                //Log.d("Status", "StatVal: " + jsonObject)
                var status = jsonObject.get("Status")
                Log.d("st", "st: " + status)
                if (status.equals("ok")) {
                    var responseValue = jsonObject.get("Responsvalue")
                    var arrayLength = jsonObject.get("RowNumber")
                    var ar = JSONArray("[" + responseValue.toString() + "]")

                    var arr = ar.getJSONArray(0)
                    Log.d("gg", "gg: " + arr)

                    teamNN = arrayOfNulls<String>(arrayLength as Int)
                    teamPP = arrayOfNulls<String>(arrayLength as Int)
                    checkII = arrayOfNulls<String>(arrayLength as Int)
                    volII = arrayOfNulls<String>(arrayLength as Int)
                    microII = arrayOfNulls<String>(arrayLength as Int)
                    positionII = IntArray(arrayLength as Int)

                    for (i in 0 until arrayLength as Int) {
                        //Log.d("loopjjjjjjjjjj", "loop: " + i)
                        positionII[i] = arr.getJSONObject(i).getInt("id")
                        //Log.d("positionII", "positionII: " + positionII[i])
                        teamNN[i] = arr.getJSONObject(i).getString("team_name")
                        //Log.d("team_name", "team_name: " + teamNN[i])
                        teamPP[i] = arr.getJSONObject(i).getString("team_place")
                        //Log.d("teamP", "teamP: " + teamPP[i])
                        volII[i] = arr.getJSONObject(i).getString("volume")
                        //Log.d("teamV", "teamV: " + volII[i])
                        microII[i] = arr.getJSONObject(i).getString("microphone")
                        //Log.d("teamM", "teamM: " + microII[i])
                        checkII[i] = arr.getJSONObject(i).getString("checkbox")
                        //Log.d("teamc", "teamc: " + checkII[i])

                        arrayListviewAdapter.add(ItemList(teamNN[i], teamPP[i], microII[i], volII[i], checkII[i], i))
                    }
                    //progressDialog.dismiss()
                    listDataAdapter = MyCustomListAdapter(context, R.layout.sample_view, arrayListviewAdapter, microII, volII, checkII, positionII)
                    listView!!.adapter = listDataAdapter
                    //Log.d("ARRAYLIS","SIZE: "+arrayListviewAdapter.size)


                } else {
                    //progressDialog.dismiss()
                    Toast.makeText(context, "Error else", Toast.LENGTH_SHORT).show();
                }

            }, Response.ErrorListener { e ->
                //progressDialog.dismiss()
                Toast.makeText(context, "Error response", Toast.LENGTH_SHORT).show();
                Log.d("ErrorListener", "ErrorListener" + e.message)
            }) {
                override fun getParams(): Map<String, String> {
                    var params = HashMap<String, String>()
                    params.put("content", searchText)
                    return params
                }
            }
            var requestQueue = Volley.newRequestQueue(context)
            requestQueue.add<String>(stringRequest)
        }
    }

    //to update microphone data
    private fun callUpdateMicroApi() {
        val baseUrlLogIn = "http://192.168.207.24/update_microphone.php?Id="+Id+"&microphoneId="+volMicCheckId
        Log.d("URL", "callUpdateMicroApi : "+baseUrlLogIn)

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Listener { s ->
            Log.d("SUCCESS", "callUpdateMicroApi: " + s)
            var jsonObject = JSONObject(s)
            Log.d("Status", "StatVal: " + jsonObject)
            var status=jsonObject.get("Status")
            Log.d("st", "st: " + status)
            if(status.equals("ok")){


            }else{
                //progressDialog.dismiss()
                Toast.makeText(context, "Error else", Toast.LENGTH_SHORT).show();
            }

        }, Response.ErrorListener { e ->
            //progressDialog.dismiss()
            Toast.makeText(context, "Error response", Toast.LENGTH_SHORT).show();
            Log.d("ErrorListener","ErrorListener"+e.message)
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                params.put("Id",Id)
                params.put("microphoneId",volMicCheckId)
                return  params
            }
        }
        val  requestQueue = Volley.newRequestQueue(context)
        requestQueue.add<String>(stringRequest)

    }

    //to update volume data
    private fun callUpdateVolumeApi() {

        val baseUrlLogIn = "http://192.168.207.24/update_volume.php?Id="+Id+"&volumeId="+volMicCheckId
        Log.d("URL", "callUpdateVolumeApi : "+baseUrlLogIn)

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Listener { s ->
            Log.d("SUCCESS", "callUpdateVolumeApi: " + s)
            var jsonObject = JSONObject(s)
            Log.d("Status", "StatVal: " + jsonObject)
            var status=jsonObject.get("Status")
            Log.d("st", "st: " + status)
            if(status.equals("ok")){


            }else{
                //progressDialog.dismiss()
                Toast.makeText(context, "Error else", Toast.LENGTH_SHORT).show();
            }

        }, Response.ErrorListener { e ->
            //progressDialog.dismiss()
            Toast.makeText(context, "Error response", Toast.LENGTH_SHORT).show();
            Log.d("ErrorListener","ErrorListener"+e.message)
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                params.put("Id",Id)
                params.put("microphoneId",volMicCheckId)
                return  params
            }
        }
        val  requestQueue = Volley.newRequestQueue(context)
        requestQueue.add<String>(stringRequest)

    }

    //to update checkbox data
    private fun callUpdateCheckApi() {
        val baseUrlLogIn = "http://192.168.207.24/update_checkbox.php?Id="+Id+"&checkboxId="+volMicCheckId
        Log.d("URL", "callUpdateCheckApi : "+baseUrlLogIn)

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Listener { s ->
            Log.d("SUCCESS", "callUpdateCheckApi: " + s)
            var jsonObject = JSONObject(s)
            Log.d("Status", "StatVal: " + jsonObject)
            var status=jsonObject.get("Status")
            Log.d("st", "st: " + status)
            if(status.equals("ok")){


            }else{
                //progressDialog.dismiss()
                Toast.makeText(context, "Error else", Toast.LENGTH_SHORT).show();
            }

        }, Response.ErrorListener { e ->
            //progressDialog.dismiss()
            Toast.makeText(context, "Error response", Toast.LENGTH_SHORT).show();
            Log.d("ErrorListener","ErrorListener"+e.message)
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                params.put("Id",Id)
                params.put("microphoneId",volMicCheckId)
                return  params
            }
        }
        val  requestQueue = Volley.newRequestQueue(context)
        requestQueue.add<String>(stringRequest)
    }

    private fun callDefaultAllOffApi() {
        val baseUrlLogIn = "http://192.168.207.24/app_start.php?data="
        Log.d("URL", "callSelectAllCheckBoxDataApi : "+baseUrlLogIn)

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Listener { s ->
            Log.d("SUCCESS", "callSelectAllCheckBoxDataApi: " + s)
            var jsonObject = JSONObject(s)
            //Log.d("Status", "StatVal: " + jsonObject)
            var status=jsonObject.get("Status")
            //Log.d("st", "st: " + status)
            if(status.equals("ok")){
                //Toast.makeText(context, "status ok", Toast.LENGTH_SHORT).show();

            }else{
                //progressDialog.dismiss()
                Toast.makeText(context, "Error else", Toast.LENGTH_SHORT).show();
            }

        }, Response.ErrorListener { e ->
            //progressDialog.dismiss()
            Toast.makeText(context, "Error response", Toast.LENGTH_SHORT).show();
            Log.d("ErrorListener","ErrorListener"+e.message)
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                return  params
            }
        }
        val  requestQueue = Volley.newRequestQueue(context)
        requestQueue.add<String>(stringRequest)
    }

    /*
    //currently no need
     */
    //to go second activity
    private fun callSecondActivityApi() {
        val baseUrlLogIn = "http://192.168.207.24/retrieve_by_id.php?Id="+Id
        //Log.d("URL", "callUpdateCheckApi: "+baseUrlLogIn)

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Listener { s ->
           // Log.d("SUCCESS", "callUpdateCheckApi: " + s)
            var jsonObject = JSONObject(s)
            //Log.d("Status", "StatVal: " + jsonObject)
            var status=jsonObject.get("Status")
            //Log.d("st", "st: " + status)
            if(status.equals("ok")){
                var responseValue=jsonObject.get("Responsvalue")
                var ar= JSONArray("["+responseValue.toString()+"]")

                var Iposition=ar.getJSONObject(0).getInt("id")
                var Iname=ar.getJSONObject(0).getString("team_name")
                //Log.d("Iname", "Iname: " + Iname)
                var Iplace=ar.getJSONObject(0).getString("team_place")
                var Ivolume=ar.getJSONObject(0).getString("volume")
                var Imicro=ar.getJSONObject(0).getString("microphone")
                var Icheck=ar.getJSONObject(0).getString("checkbox")
               // Log.d("Icheck", "Icheck: " + Icheck)


                val intent = Intent(context, TeamDetails::class.java)
                intent.putExtra("teamName", Iname)
                intent.putExtra("teamPlace", Iplace)
                intent.putExtra("checked", Icheck)
                intent.putExtra("volume", Ivolume)
                intent.putExtra("microphone", Imicro)
                intent.putExtra("listItemPosition", Iposition)

                context.startActivity(intent)
                Toast.makeText(context,"clicked: "+Iposition,Toast.LENGTH_LONG).show();
            }else{
                //progressDialog.dismiss()
                Toast.makeText(context, "Error else", Toast.LENGTH_SHORT).show();
            }

        }, Response.ErrorListener { e ->
            //progressDialog.dismiss()
            //Toast.makeText(context, "Error response: "+e, Toast.LENGTH_SHORT).show();
            Log.d("ErrorListener","ErrorListener"+e.message)
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                return  params
            }
        }
        var  requestQueue = Volley.newRequestQueue(context)
        requestQueue.add<String>(stringRequest)
    }
}