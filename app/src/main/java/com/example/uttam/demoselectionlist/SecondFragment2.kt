package com.example.uttam.demoselectionlist

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Fragment
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import android.content.Context
import android.support.v4.provider.FontsContractCompat.FontRequestCallback.RESULT_OK
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v4.app.FragmentActivity
import android.app.Activity




public class SecondFragment2 : Fragment() {

    private var rootView: View? = null
    private var adapter: ArrayAdapter<ItemList>? = null
    private var TeamItem: MutableList<ItemList>? = null
    var lv: ListView?=null
    internal var mAllData = ArrayList<ItemList>()
    internal var m:Int=0
    internal var listItemClicked:Int=0
    lateinit var teamList: Array<String?>
    lateinit var teamPlace: Array<String?>
    lateinit var selectAllCheck: ImageView
    lateinit var selectAllVolume: ImageView
    lateinit var selectAllMicrophone: ImageView
    internal var searchView: SearchView? = null
    lateinit var checkedIcon: Array<String?>
    lateinit var volumeIcon: Array<String?>
    lateinit var microphoneIcon: Array<String?>
    //lateinit var et: EditText
    lateinit var itemPosition: IntArray
    internal var currentPosition: ItemList? = null
    lateinit var cont: Context
    lateinit var listener:FragmentActivity


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        m=rowCount()

        //Random random = new Random();
        //int m = random.nextInt(50 - 5) + 5;
        teamList = arrayOfNulls<String>(m)
        teamPlace = arrayOfNulls<String>(m)
        checkedIcon = arrayOfNulls<String>(m)
        volumeIcon = arrayOfNulls<String>(m)
        microphoneIcon = arrayOfNulls<String>(m)
        itemPosition = IntArray(m)

        rootView = inflater.inflate(R.layout.fragment_second, container, false)
        lv = rootView!!.findViewById<ListView>(R.id.teamsListViewId) as ListView
        selectAllCheck = rootView!!.findViewById<ImageView>(R.id.selectAllCheckId)
        selectAllVolume = rootView!!.findViewById<ImageView>(R.id.selectAllVolumeId)
        selectAllMicrophone = rootView!!.findViewById<ImageView>(R.id.selectAllMicroId)

        //Log.d("listviewId", "listviewId sssss: " + lv!!.getId())

        enableSelectAllVolBtn()
        enableSelectAllMicBtn()
        enableSelectAllGpsBtn()
        retrieveAllData()


        //for select all volume icon
        selectAllVolume.setOnClickListener(View.OnClickListener {
            if (selectAllVolume.getDrawable().getConstantState() === resources.getDrawable(R.drawable.icon_checkbox_false).constantState) {
                select_all( "1","select_all_volume")
                selectAllVolume.setImageResource(R.drawable.icon_checkbox)
                for (i in 0 until m) {
                    if (volumeIcon[i]=="0") {
                        volumeIcon[i] = "1"
                    }
                }

            } else if(selectAllVolume.getDrawable().getConstantState() === resources.getDrawable(R.drawable.icon_checkbox).constantState)  {
                select_all( "0","select_all_volume")
                selectAllVolume.setImageResource(R.drawable.icon_checkbox_false)
                for (i in 0 until m) {
                    if (volumeIcon[i]=="1") {
                        volumeIcon[i] = "0"
                    }
                }
            }
        })


        //for select all microphone icon
        selectAllMicrophone.setOnClickListener(View.OnClickListener {
            if (selectAllMicrophone.getDrawable().getConstantState() === resources.getDrawable(R.drawable.icon_checkbox_false).constantState) {
                select_all( "1","select_all_microphone")
                selectAllMicrophone.setImageResource(R.drawable.icon_checkbox)
                for (i in 0 until m) {
                    if (microphoneIcon[i]=="0") {
                        microphoneIcon[i] = "1"
                    }
                }

            } else if(selectAllMicrophone.getDrawable().getConstantState() === resources.getDrawable(R.drawable.icon_checkbox).constantState)  {
                select_all( "0","select_all_microphone")
                selectAllMicrophone.setImageResource(R.drawable.icon_checkbox_false)
                for (i in 0 until m) {
                    if (microphoneIcon[i]=="1") {
                        microphoneIcon[i] = "0"
                    }
                }
            }
        })


        //for select all checkbox icon
        selectAllCheck.setOnClickListener(View.OnClickListener {
            if (selectAllCheck.getDrawable().getConstantState() === resources.getDrawable(R.drawable.icon_checkbox_false).constantState) {
                select_all( "1","select_all_gps")
                selectAllCheck.setImageResource(R.drawable.icon_checkbox)
                for (i in 0 until m) {
                    if (checkedIcon[i]=="0") {
                        checkedIcon[i] = "1"
                    }
                }

            } else if(selectAllCheck.getDrawable().getConstantState() === resources.getDrawable(R.drawable.icon_checkbox).constantState)  {
                select_all( "0","select_all_gps")
                selectAllCheck.setImageResource(R.drawable.icon_checkbox_false)
                for (i in 0 until m) {
                    if (checkedIcon[i]=="1") {
                        checkedIcon[i] = "0"
                    }
                }
            }
        })

        //listitem click listener
        lv!!.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            Log.d("second_activity","second_activity"+l)
            second_activity(i)
        }

        return rootView
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 2 && resultCode == RESULT_OK) {
            //Log.d("onActivityResult","true: ")
        }else{
            retrieveAllData()
            //Log.d("onActivityResult","false")
        }
    }

    fun retrieveAllData(){
        var methodName="retrieveAllData"
        var apiHelper=ApiHelper(activity.applicationContext, lv!!, methodName)
        apiHelper.execute()
    }

    fun select_all(data: String, methodName:String) {
        var apiHelper = ApiHelper(activity.applicationContext, data, methodName)
        apiHelper.execute()
        Thread.sleep(200)
        retrieveAllData()
    }

    fun second_activity(pId: Int) {
        val baseUrlLogIn = "http://192.168.207.24/retrieve_by_id.php?Id="+(pId+1)
        //Log.d("URL", "callLoginApi: baseUrlLogIn: "+baseUrlLogIn)

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Response.Listener { s ->
            // Log.d("SUCCESS", "RequestSuccessVal: " + s)
            var jsonObject = JSONObject(s)
            //Log.d("Status", "StatVal: " + jsonObject)
            var status = jsonObject.get("Status")
            //Log.d("st", "st: " + status)
            if (status.equals("ok")) {
                var responseValue = jsonObject.get("Responsvalue")
                var ar = JSONArray("[" + responseValue.toString() + "]")

                var Iposition = ar.getJSONObject(0).getInt("id")
                var Iname = ar.getJSONObject(0).getString("team_name")
                //Log.d("Iname", "Iname: " + Iname)
                var Iplace = ar.getJSONObject(0).getString("team_place")
                var Ivolume = ar.getJSONObject(0).getString("volume")
                var Imicro = ar.getJSONObject(0).getString("microphone")
                var Icheck = ar.getJSONObject(0).getString("checkbox")
                // Log.d("Icheck", "Icheck: " + Icheck)


                val intent = Intent(activity.applicationContext, TeamDetails::class.java)
                intent.putExtra("teamName", Iname)
                intent.putExtra("teamPlace", Iplace)
                intent.putExtra("checked", Icheck)
                intent.putExtra("volume", Ivolume)
                intent.putExtra("microphone", Imicro)
                intent.putExtra("listItemPosition", Iposition)

                startActivityForResult(intent,1)

                //activity.applicationContext.startActivity(intent)
               // Toast.makeText(activity.applicationContext, "clicked: " + Iposition, Toast.LENGTH_LONG).show();
            } else {
                //progressDialog.dismiss()
                Toast.makeText(activity.applicationContext, "Error else", Toast.LENGTH_SHORT).show();
            }

        }, Response.ErrorListener { e ->
            //progressDialog.dismiss()
            Toast.makeText(activity.applicationContext, "Error response: "+e, Toast.LENGTH_SHORT).show();
            //Log.d("ErrorListener","ErrorListener"+e.message)
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                return  params
            }
        }
        var  requestQueue = Volley.newRequestQueue(activity.applicationContext)
        requestQueue.add<String>(stringRequest)

    }

    fun enableSelectAllGpsBtn() {
        val baseUrlLogIn = "http://192.168.207.24/select_all_gps_btn.php?"

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Response.Listener { s ->
            var jsonObject = JSONObject(s)
            //Log.d("Status", "StatVal: " + jsonObject)
            var status=jsonObject.get("Status")
            //Log.d("st", "st: " + status)
            if(status.equals("ok")){
                var responseValue=jsonObject.get("Responsevalue")
                var ar= JSONArray("["+responseValue.toString()+"]")

                var enable=ar.getJSONObject(0).getString("location_enable")
                if (enable.equals("1")){
                    selectAllCheck.setImageResource(R.drawable.icon_checkbox)

                }else{
                    selectAllCheck.setImageResource(R.drawable.icon_checkbox_false)
                }
            }else{
                //progressDialog.dismiss()
                Toast.makeText(activity.applicationContext, "Error else", Toast.LENGTH_SHORT).show();
            }

        }, Response.ErrorListener { e ->
            Toast.makeText(activity.applicationContext, "Error response: "+e, Toast.LENGTH_SHORT).show()
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                return  params
            }
        }
        var  requestQueue = Volley.newRequestQueue(activity.applicationContext)
        requestQueue.add<String>(stringRequest)
    }

    fun enableSelectAllVolBtn() {

        val baseUrlLogIn = "http://192.168.207.24/select_all_vol_btn.php?"

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Response.Listener { s ->
            var jsonObject = JSONObject(s)
            //Log.d("Status", "StatVal: " + jsonObject)
            var status=jsonObject.get("Status")
            //Log.d("st", "st: " + status)
            if(status.equals("ok")){
                var responseValue=jsonObject.get("Responsevalue")
                var ar= JSONArray("["+responseValue.toString()+"]")

                var enable=ar.getJSONObject(0).getString("volume_enable")
                if (enable.equals("1")){
                    selectAllVolume.setImageResource(R.drawable.icon_checkbox)

                }else{
                    selectAllVolume.setImageResource(R.drawable.icon_checkbox_false)
                }
            }else{
                //progressDialog.dismiss()
                Toast.makeText(activity.applicationContext, "Error else", Toast.LENGTH_SHORT).show();
            }

        }, Response.ErrorListener { e ->
            Toast.makeText(activity.applicationContext, "Error response"+e, Toast.LENGTH_SHORT).show()
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                return  params
            }
        }
        var  requestQueue = Volley.newRequestQueue(activity.applicationContext)
        requestQueue.add<String>(stringRequest)
    }

    fun enableSelectAllMicBtn() {
        val baseUrlLogIn = "http://192.168.207.24/select_all_mic_btn.php?"

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Response.Listener { s ->
            var jsonObject = JSONObject(s)
            //Log.d("Status", "StatVal: " + jsonObject)
            var status=jsonObject.get("Status")
            //Log.d("st", "st: " + status)
            if(status.equals("ok")){
                var responseValue=jsonObject.get("Responsevalue")
                var ar= JSONArray("["+responseValue.toString()+"]")

                var enable=ar.getJSONObject(0).getString("microphone_enable")
                if (enable.equals("1")){
                    selectAllMicrophone.setImageResource(R.drawable.icon_checkbox)

                }else{
                    selectAllMicrophone.setImageResource(R.drawable.icon_checkbox_false)
                }
            }else{
                //progressDialog.dismiss()
                Toast.makeText(activity.applicationContext, "Error else", Toast.LENGTH_SHORT).show();
            }

        }, Response.ErrorListener { e ->
            Toast.makeText(activity.applicationContext, "Error response"+e, Toast.LENGTH_SHORT).show()
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                return  params
            }
        }
        var  requestQueue = Volley.newRequestQueue(activity.applicationContext)
        requestQueue.add<String>(stringRequest)
    }

    private fun rowCount(): Int {
        var rowNumber:String="0"

        val baseUrlLogIn = "http://192.168.207.24/row_number.php?"

        val stringRequest = object : StringRequest(Request.Method.POST, baseUrlLogIn, Response.Listener { s ->
            var jsonObject = JSONObject(s)
            rowNumber = jsonObject.get("RowNumber").toString()

        }, Response.ErrorListener { e ->
            Toast.makeText(activity.applicationContext, "Error response"+e, Toast.LENGTH_SHORT).show()
            //Toast.makeText(activity.applicationContext, "Error response", Toast.LENGTH_SHORT).show()
        }) {
            override fun getParams(): Map<String, String> {
                var params= HashMap<String,String>()
                return  params
            }
        }
        var  requestQueue = Volley.newRequestQueue(activity.applicationContext)
        requestQueue.add<String>(stringRequest)

        var row:Int=rowNumber.toInt()

       return row
    }

    fun toast(){
        Log.d("log", "logggg: " )
//        enableSelectAllBtn()
    }

}

