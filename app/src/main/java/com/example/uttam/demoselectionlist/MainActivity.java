package com.example.uttam.demoselectionlist;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Locale;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {

    ImageView mainMenuFragment, teamsFragment, logo;
    SearchView searchView;
    RelativeLayout fragmentIcon;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    int teamD=0;
    String shareZero="0";
    String shareOne="1";
    String fragLoad;
    int orientation;
    String s;
    ListView lv;
    ListAdapter adapter;
    View rootview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        View secondLayout = getLayoutInflater().inflate(R.layout.fragment_second, null, false);
        lv= (ListView) secondLayout.findViewById(R.id.teamsListViewId);
        //Log.d("listviewId", "listviewId: "+lv.getId());

        //get orientation from here, if it returns 1 then it is portrait if returns 2 then it is landscape
        orientation = getResources().getConfiguration().orientation;

        // get the reference of Button's
        mainMenuFragment = (ImageView) findViewById(R.id.firstFragment);
        teamsFragment = (ImageView) findViewById(R.id.secondFragment);

        fragmentIcon=findViewById(R.id.fragmentIconId);
        searchView=findViewById(R.id.searchViewId);
        logo=findViewById(R.id.logoId);

        pref = this.getSharedPreferences("MyPref", MODE_PRIVATE);
        s=pref.getString("r", "0");
        //editor = pref.edit();

        Log.d("TAGaaa", "orientation: "+orientation+" r: "+s);
        if(orientation==1&&s=="1"){
            logo.setVisibility(View.GONE);
            fragmentIcon.setVisibility(View.GONE);
        }else{
            logo.setVisibility(View.VISIBLE);
            fragmentIcon.setVisibility(View.VISIBLE);
        }

        //properties of searchview
        int id = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView.findViewById(id);
        textView.setTextColor(Color.WHITE);
        textView.setHint("Search here");
        textView.setHintTextColor(Color.WHITE);


        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor = pref.edit();
                editor.putString("r", shareOne);
                editor.apply();
                editor.commit();

                s=pref.getString("r", "0");
                Log.d("TAGaaaOnS", "orientation: "+orientation+" r: "+s);
                //Log.d("TAG", "setOnSearchClickListener: "+s);
                logo.setVisibility(View.GONE);
                fragmentIcon.setVisibility(View.GONE);
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                editor = pref.edit();
                editor.putString("r", shareZero);
                editor.apply();
                editor.commit();
                s=pref.getString("r", "0");
                Log.d("TAGaaaOnC", "orientation: "+orientation+" r: "+s);
                logo.setVisibility(View.VISIBLE);
                fragmentIcon.setVisibility(View.VISIBLE);
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                search(s);
                return false;
            }
        });

//        fragLoad=pref.getString("frag", "0");
//        Log.d("TAGaaaF", "fragLoad: "+fragLoad);
//        if(fragLoad.equals("0")){
            mainMenuFragment.setImageResource(R.drawable.icon_home_blue);
            teamsFragment.setImageResource(R.drawable.icon_team);
            loadFragment(new FirstFragment());
            searchView.setVisibility(View.GONE);
//        }else if(fragLoad.equals("1")) {
//            searchView.setVisibility(View.VISIBLE);
//            mainMenuFragment.setImageResource(R.drawable.icon_home);
//            teamsFragment.setImageResource(R.drawable.icon_team_blue);
//            loadFragment(new SecondFragment2());
//        }


        defaultAllOff();

        Intent intent=getIntent();
        teamD=intent.getIntExtra("teamD",0);

        if(teamD==0){
            mainMenuFragment.setImageResource(R.drawable.icon_home_blue);
            teamsFragment.setImageResource(R.drawable.icon_team);
            loadFragment(new FirstFragment());
        }else if(teamD==1){
            mainMenuFragment.setImageResource(R.drawable.icon_home);
            teamsFragment.setImageResource(R.drawable.icon_team_blue);
            loadFragment(new SecondFragment2());
            teamD=0;
        }

        mainMenuFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                editor = pref.edit();
//                editor.putString("frag", "0");
//                editor.commit();
//                fragLoad=pref.getString("frag", "0");
//                Log.d("TAGaaaFMain", "fragLoad: "+fragLoad);
                searchView.setVisibility(View.GONE);
                mainMenuFragment.setImageResource(R.drawable.icon_home_blue);
                teamsFragment.setImageResource(R.drawable.icon_team);
                loadFragment(new FirstFragment());
            }
        });

        teamsFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setVisibility(View.VISIBLE);
                mainMenuFragment.setImageResource(R.drawable.icon_home);
                teamsFragment.setImageResource(R.drawable.icon_team_blue);
                loadFragment(new SecondFragment2());
            }
        });
    }

    private void defaultAllOff() {
        String methodName="defaultAllOff";
        ApiHelper apiHelper= new ApiHelper(this,methodName);
        apiHelper.execute();
    }


    private void search(String s) {
        String methodName="search";
        ApiHelper apiHelper= new ApiHelper(this,lv,methodName,s);
        apiHelper.execute();
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    @Override
    public void onBackPressed() {
        if(teamsFragment.getDrawable().getConstantState() == getResources().getDrawable( R.drawable.icon_team_blue).getConstantState()){
            mainMenuFragment.setImageResource(R.drawable.icon_home_blue);
            teamsFragment.setImageResource(R.drawable.icon_team);
            loadFragment(new FirstFragment());
        }else{
            super.onBackPressed();
        }
    }

}

