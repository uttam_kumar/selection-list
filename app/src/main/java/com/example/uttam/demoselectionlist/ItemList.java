package com.example.uttam.demoselectionlist;

public class ItemList {

    String tName;
    String tPlace;
    String tMicro;
    String tVolume;
    String tCheckbox;
    int tItemPosition;

    public ItemList(String tName, String tPlace, String tMicro, String tVolume, String tCheckbox,int tItemPosition) {
        this.tName=tName;
        this.tPlace=tPlace;
        this.tMicro=tMicro;
        this.tVolume=tVolume;
        this.tCheckbox=tCheckbox;
        this.tItemPosition=tItemPosition;

    }

    public String gettMicro() {
        return tMicro;
    }

    public String gettName() {
        return tName;
    }

    public String gettPlace() {
        return tPlace;
    }

    public String gettVolume() {
        return tVolume;
    }

    public String gettCheck() {
        return tCheckbox;
    }

    public int gettItemPosition() {
        return tItemPosition;
    }
}
